using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Gen : MonoBehaviour
{
    [SerializeField]
    float floorH, floorW = 0;
    public GameObject Part_Point;
    public GameObject Part_Floor_Cube;
    public int room_Number;

    public GameObject Part_Wall_U, Part_Wall_D ,Part_Wall_L ,Part_Wall_R;

    //private Floor_Script Floor_Script;
    //private Room_Gen room_Gen;
    private Spawn_On_Spawner Spawn_On_Spawner;

    public void Start()
    {
        floorW = Random.Range(8,15); floorH = Random.Range(6,18);
  //      Floor_Script.room_Number = Spawn_On_Spawner.totalRooms;
      //  Floor_Script.room_Number = Random.Range(1,4);
             // Spawn_On_Spawner = new Spawn_On_Spawner();
        //room_Number = Spawn_On_Spawner.totalRooms;

        Spawn_On_Spawner = new Spawn_On_Spawner();
        Debug.Log(Spawn_On_Spawner.totalRooms);
        Wall_Gen_Method();
    }

    void Wall_Gen_Method()
    { // reminder that scale X is floor W and scale Z is floor H
       // this.Floor_Script.room_Number = Random.Range(1,4);
        float posX = this.transform.position.x;
        float posY = this.transform.position.y;
        float posZ = this.transform.position.z;
        float posXa = ((floorW/2)+0.5f); 
        float posZa = ((floorH/2)+0.5f); 
// it used to be like ((scaleX/2)+0.5f) because for some reason it never occured to me that i could access them directly since they're in the same class
        Part_Floor_Cube.transform.localScale = new Vector3(floorW, 1, floorH);

        for (int Wall_Count = 0; Wall_Count < 4; Wall_Count++)
        { 
            switch (Wall_Count) 
            { 
                case 0:
                    Debug.Log("Wall Generation for room Starting");
                    Part_Wall_U.transform.localScale = new Vector3(floorW,1,1);
                    Instantiate(Part_Wall_U, new Vector3(posX,posY,posZ+posZa), transform.rotation, this.transform);    
                    break;
                case 1:
                    Part_Wall_D.transform.localScale = new Vector3(floorW,1,1);
                    Instantiate(Part_Wall_D, new Vector3(posX,posY,posZ-posZa), transform.rotation, this.transform);
                    break;
                case 2:
                    Part_Wall_L.transform.localScale = new Vector3(1,1,floorH);
                    Instantiate(Part_Wall_L, new Vector3(posX-posXa,posY,posZ), transform.rotation, this.transform);   
                    break;
                case 3:
                    Part_Wall_R.transform.localScale = new Vector3(1,1,floorH);
                    Instantiate(Part_Wall_R, new Vector3(posX+posXa,posY,posZ), transform.rotation, this.transform);
                    Debug.Log("Wall Generation for room Completed");
                    break;
                default:    
                    Debug.Log("you shouldn't be here");
                    break;
            }          
        }
    }
}