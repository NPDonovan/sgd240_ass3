using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_On_Spawner : MonoBehaviour
{
    public GameObject floor;
    public bool spawnRoom = false;
    public bool roomSpread = true;
    public int roomCount = 1;
    public int roomNumber = 1;
    public int totalRooms = 0;

   // private Floor_Script Floor_Script;
   
    void Update()
    {
        if (spawnRoom)
        {
            spawnRoom=false;
            if(roomSpread)
            {
                FloorSpawnSpread(roomCount);
            }
            else
            {
                FloorSpawnWiggle(roomCount);
            }
            Debug.Log("Click");
        }
    }

    void FloorSpawnSpread(int _roomCount)
    {
      //  Floor_Script = new Floor_Script();

        int room_Count_Max = _roomCount; 
        int room_Count_Current;
        float posX_001 = 0;
        float posY_001 = 0;
        float posZ_001 = 0;

        Vector3 floorPos = new Vector3(posX_001,posY_001,posZ_001);

        for (room_Count_Current = 0; room_Count_Current < room_Count_Max; room_Count_Current++)
        {
            totalRooms = totalRooms+1;            
            Instantiate(floor, floorPos, transform.rotation);

            if (floorPos.x<100)
            {
                floorPos.Set((posX_001+=20),posY_001,posZ_001);
            }
            else
            {
                floorPos.Set((posX_001=0),posY_001,(posZ_001-=20));
            }
            Debug.Log("Room Done. Current Room count: "+(room_Count_Current+1));
            //Debug.Log("Room_Number should be " + Floor_Script.room_Number);
        }
    }

    void FloorSpawnWiggle(int _roomCount)
    {
        int room_Count_Max = _roomCount; 
        int room_Count_Current;
        float posX = 0;
        float posY = 0;
        float posZ = 0;

        Vector3 floorPos = new Vector3(posX,posY,posZ);

        for (room_Count_Current = 0; room_Count_Current < room_Count_Max; room_Count_Current++)
        {
            Instantiate(floor, floorPos, transform.rotation);
            //cube_floor.floor_script.room_number = room_Count_Current
            Debug.Log("Room Done. Current Room count: " + (room_Count_Current+1));
        }
    }

}